#version 120

//Un Fragment Shader minimaliste
void main (void)
{
    //couleur du fragment
    float x = (gl_FragCoord.x/600.0)-0.5;
    float y = (gl_FragCoord.y/600.0)-0.5;
    float rayon = 0.2;
    float r,g;
    if((x*x+y*y)<(rayon*rayon)) {
        r = 1.0;
        g = 0.0;
    }
    else {
        r = 0.0;
        g = 1.0;
    }
    gl_FragColor = vec4(r, g, 0.0, 1.0);    
}
