#version 120

varying vec3 coordonnee_3d;

//Un Fragment Shader minimaliste
void main (void)
{
    float x = abs(coordonnee_3d.x);
    float y = abs(coordonnee_3d.y);
    float z = abs(coordonnee_3d.z);

    float r = (1.0+1.0-sqrt(2.0))/2.0;
    float r2= r/2.0;
    float xp = x - r;
    float yp = y - r;

    if(((xp*xp)+(yp*yp))<(r2*r2)) {
        discard;
    }
    else if(((xp*xp)+(yp*yp))<(r*r)) {
        gl_FragColor = vec4(1.0-x, 1.0-y, 1.0-z, 1.0);
    }
    else {
        gl_FragColor = vec4(x,y,z,1.0);
    }


}
